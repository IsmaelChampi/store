<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>


   <style> p{font-family:'Raleway',sans-serif}h1{font-family:'Raleway',sans-serif}li{font-family:'Raleway',sans-serif}section{display:block}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.flexbox1{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.box1{flex-shrink:0;width:100%}.list{list-style:none}.slider{width:100%;margin:auto;overflow:hidden}.slider ul{display:flex;padding:0;width:400%;animation:cambio 20s infinite alternate linear}.slider li{width:100%;list-style:none}.img{width:100%}@keyframes cambio{0%{margin-left:0}20%{margin-left:0}25%{margin-left:-100%}45%{margin-left:-100%}50%{margin-left:-200%}70%{margin-left:-200%}75%{margin-left:-300%}100%{margin-left:-300%}}.flexbox1{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.box1{flex-shrink:0;width:100%}.div1{width:100%}.flexbox2{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.div2{width:100%}.container1{margin:0 auto;width:1100px}.blue2{color:#498bcf}.flexbox3{display:flex;flex-wrap:wrap;align-items:center;justify-content:center}.box3{flex-shrink:0;width:50%}.image3{width:100%}.img3{width:50%;padding:15px}.div3-1{width:50%;float:right;background:#498bcf}.date{border-bottom:1px solid #f0f0f0;color:#498bcf;padding:20px 0;position:relative;font-size:28px;width:90%}.date2{padding:20px 0;font-size:28px;width:80%;border-bottom:solid 1px #D5D8DC;color:white;width:80%}.list3{list-style:none}.p-list{border-bottom:solid 1px #D5D8DC}ul.tabs{margin:0px;padding:0px;list-style:none;width:100%}ul.tabs li{background:none;color:#222;display:inline-block;padding:10px 15px}ul.tabs li.current{background:#498bcf;color:white}.tab-content{display:none;background:#FDFEFE;padding:15px;width:100%}.tab-content.current{display:inherit}.btn3{background-color:#498bcf;color:white;float:right;margin-top:-35px;padding:0;border-radius:6px}@media screen and (max-width:795px){.container1{margin:0 auto;width:100%}}@media screen and (max-width:480px){.box3{flex-shrink:0;width:100%}.div3-1{width:100%;float:right;background:#498bcf}}
     </style>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  

  </head>
  <body>
       <section id="">
       <div class="flexboxs">
    <div class="boxs">
      <div class="slider">
      <ul>
        <li class="lists">
  <img src="<?php echo get_template_directory_uri(); ?>/img/1.jpg" class="imgs" alt="">
 </li>
        <li class="lists">
  <img src="<?php echo get_template_directory_uri(); ?>/img/1-1.jpg" class="imgs" alt="">
</li>
      </ul>
    </div>
   </div>
     </div>
   </section>
   <br>
      <section id="">
       <div class="flexboxs1">
    <div class="boxs1">
      <div class="divs1">
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.932094641998!2d-98.1267903853867!3d19.06672298709321!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfea7a93931a5f%3A0xff14e798ed71865d!2sCalle+10-J%2C+Bosques+de+San+Sebasti%C3%A1n%2C+72310+Puebla%2C+Pue.!5e0!3m2!1ses-419!2smx!4v1565722411828!5m2!1ses-419!2smx" width="100%"  frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    </div>
     </div>
    </section>
    <br>
    <br>
<div class="container1">
    <section id="">
       <div class="flexboxs2">
      <div class="divs2">
      <h1 class="blues2">Rome Museum Tickets And Tours</h1>
      <br>
      <p><?php echo get_post_meta(get_the_ID(),'descrip',TRUE); ?></p>
    </div>
    </div>
     </div>
    </section>
<div class="container1">
      <section id="">
       <div class="flexboxs3">
        <!-- <div class="container1"> -->
    <div class="boxs3">
    <h1 class="dates">DATE & PRICES</h1>
    <br>
  <ul class="tabs">
    <li class="tab-link current" data-tab="tab-1">VIEW ALL</li>
    <li class="tab-link" data-tab="tab-2">SPRING</li>
    <li class="tab-link" data-tab="tab-3">SUMMER</li>
  </ul>

  <div id="tab-1" class="tab-content current">
     <ul>
        <li class="lists3">
        <p class="p-list">March 29-April 6 (Sunday-Monday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
        <li class="list">
        <p class="p-list">April 10-April 18 (Friday-Saturday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
         <li class="list">
        <p class="p-list">April 12-April 20 (Sunday-Monday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
        <li class="list">
        <p class="p-list">May 1-April 9 (Friday-Saturday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
         <li class="list">
        <p class="p-list">May 24-April 1 (Sunday-Monday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
        <li class="list">
        <p class="p-list">June 2-June 10 (Monday-Thursday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
      </ul>
  </div>
  <div id="tab-2" class="tab-content">
     <ul>
        <li class="lists3">
        <p class="ps-list">March 29-April 6 (Sunday-Monday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
        <li class="list">
        <p class="ps-list">April 10-April 18 (Friday-Saturday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
         <li class="list">
        <p class="ps-list">April 12-April 20 (Sunday-Monday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
      </ul>
  </div>
  <div id="tab-3" class="tab-content">
    <ul>
        <li class="lists3">
        <p class="ps-list">May 1-April 9 (Friday-Saturday)</p>
        <button type="button" class="btns3 btn-outline-warning">BOOK NOM</button>
        </li>
        <li class="list">
        <p class="ps-list">May 24-April 1 (Sunday-Monday)</p>
        <button type="button" class="btn3 btn-outline-warning">BOOK NOM</button>
        </li>
         <li class="list">
        <p class="ps-list">June 2-June 10 (Monday-Thursday)</p>
        <button type="button" class="btn3 btn-outline-warning">BOOK NOM</button>
        </li>
      </ul>
  </div>
<!-- container -->
    </div>
    <div class="divs3-1">
    <center><h1 class="dates2">Packages<br>Includes</h1></center>
    <center><ul>
      <li class="lists3">
        <i class="fas faaa icon fa-check-square"></i>
        <p class="txt3"><?php echo get_post_meta(get_the_ID(),'caracteristica1',TRUE); ?></p>
      </li>
      <li class="list3">
        <i class="fas faaa icon fa-check-square"></i>
        <p class="txt3"><?php echo get_post_meta(get_the_ID(),'caracteristica2',TRUE); ?></p>
      </li>
      <li class="list3">
        <i class="fas faaa icon fa-check-square"></i>
        <p class="txt3"><?php echo get_post_meta(get_the_ID(),'caracteristica3',TRUE); ?></p>
      </li>
       <li class="list3">
        <i class="fas faaa icon fa-check-square"></i>
        <p class="txt3"><?php echo get_post_meta(get_the_ID(),'caracteristica4',TRUE); ?></p>
      </li>
      </ul></center>
      <div class="images3">
        <center><img src="<?php echo get_template_directory_uri(); ?>/img/2.jpg" class="imgs3"></center>
      </div>
    </div>
    </div>
     </div>
    </section>
</div>
    <br>
    <br>
    <br>
    <br>
    <section id="">
       <div class="flexbox4">
    <div class="box4">
      <div class="div4">
      <div class="image4">
    <center><h1 style="color:#FDFEFE">What People Say</h1><br>
    <img src="<?php echo get_template_directory_uri(); ?>/img/cara.jpg" class="isma">
    <p style="color:#FDFEFE"><?php echo get_post_meta(get_the_ID(),'text_opcion',TRUE); ?></p></center>
</div>
    </div>
    </div>
     </div>
    </section>
    <section id="triangulo-blue">
       <div class="flexbox0">
    <div class="box0">
<div class="container1">
      <div class="div0">
        <div class="ct-0">
          <h4 class="ct-u0">ITINERARY</h4>
          <p class="colorblue0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed arcu ac ligula volutpat tincidunt vel ut mauris. Fusce nec ultrices leo.</p>

        </div>
      <div class="div0-1">
        <div class="image0">
           <img src="<?php echo get_template_directory_uri(); ?>/img/4.jpg" class="img0">
        </div>
        <div class="text0">
          <center><h1>DAY1:<br>WELCOME TO<br>ITALY</h1></center>
          <br>
          <center><p>We'll have a get-
            <br>acquainted meeting at<br>
            3 pm.at our hotel in<br>
            Rome.Loremu cres isi<br>
            ipsum dolorui sit amet,<br>
            consectetur.</p></center>
            <center><button type="button" class="btn0 btn-outline-warning">CLICK HERE</button></center>
        </div>
      </div>
      <div class="div0-2">
        <div class="image0">
           <img src="<?php echo get_template_directory_uri(); ?>/img/4-2.jpg" class="img0">
        </div>
        <div class="text0">
          <center><h1>DAY1:<br>WELCOME TO<br>ITALY</h1></center>
          <br>
          <center><p>We'll have a get-
            <br>acquainted meeting at<br>
            3 pm.at our hotel in<br>
            Rome.Loremu cres isi<br>
            ipsum dolorui sit amet,<br>
            consectetur.</p></center>
            <center><button type="button" class="btn0 btn-outline-warning">CLICK HERE</button></center>
        </div>
      </div>
    </div>
    </div>
     </div>
    </section>

   <section id="">
       <div class="flexbox5">
    <div class="box5">
      <div class="div5">
<div class="container1">
      <div class="div5-1">
        <div class="image5">
           <img src="<?php echo get_template_directory_uri(); ?>/img/5-1.jpg" class="img5">
        </div>
        <div class="text5">
          <center><h1>DAY1:<br>WELCOME TO<br>ITALY</h1></center>
          <br>
          <center><p>We'll have a get-
            <br>acquainted meeting at<br>
            3 pm.at our hotel in<br>
            Rome.Loremu cres isi<br>
            ipsum dolorui sit amet,<br>
            consectetur.</p></center>
            <center><button type="button" class="btn5 btn-outline-warning">CLICK HERE</button></center>
        </div>
      </div>
      <div class="div5-2">
        <div class="image5">
           <img src="<?php echo get_template_directory_uri(); ?>/img/5-2.jpg" class="img5">
        </div>
        <div class="text5">
          <center><h1>DAY1:<br>WELCOME TO<br>ITALY</h1></center>
          <br>
          <center><p>We'll have a get-
            <br>acquainted meeting at<br>
            3 pm.at our hotel in<br>
            Rome.Loremu cres isi<br>
            ipsum dolorui sit amet,<br>
            consectetur.</p></center>
            <center><button type="button" class="btn5 btn-outline-warning">CLICK HERE</button></center>
        </div>
      </div>
    </div>
    </div>
     </div>
    </section>
</div>
    <div class="container1">
    <section id="">
       <div class="flexboxs6">
    <div class="boxs6">
      <h1 class="bordes">REVIEWS</h1>
      <div class="divs6">
      <center><div class="images6">
           <img src="<?php echo get_template_directory_uri(); ?>/img/cara.png" class="imgs6">
           <h1 class="txtsblue">VANESSA</h1>
           <P>Williamsburg, VA<br>
            Tour: 10/26/14</P>  
        </div></center>
        <div class="texts6">
          <b>Overall rating: <i class="fas txtsblue fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas   fa-star"></i><i class="fas  fa-star"></i></b>
          <h1 class="txtsblue">Lovely experience, I gladly recommend</h1>
         <p>We had an exceptional Italy travel experience. The sites, the company of the group, the time and organization<br> management of our visits, the exceptional site tour guides, the experiences and just the sheer joy we felt being part of the<br> culture and delights of the Italian landscape and lifestyle was a memorable and delightful experience. There was not<br> anything we could suggest to change or improve the experiences.</p>
        </div>
    </div>
    </div>
      <div class="boxs6">
      <div class="divs6">
      <center><div class="images6">
           <img src="<?php echo get_template_directory_uri(); ?>/img/cara.png" class="imgs6">
           <h1 class="txtsblue">GABRIELLE</h1>
           <P>Chicago, IL<br>
            Tour: 1/21/15</P>  
        </div></center>
        <div class="texts6">
          <b>Overall rating: <i class="fas txtsblue fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas txtsblue   fa-star"></i><i class="fas txtsblue  fa-star"></i></b><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></b>
          <h1 class="txtsblue">Lovely experience, I gladly recommend</h1>
         <p>t is hard to pick a favorite because different things were wows for different reasons. The Colosseum and Forum<br> tour because of Francesca, the hike along the Mediterranean coast line for the beauty and the endurance<br> accomplishments, the time spent walking and taking in the visual sites of the architecture, Italian culture and<br> countryside in the Tuscan villages and how could I leave out the meals and wine....so many wows!</p>
        </div>
    </div>
    </div>
       <div class="boxs6">
      <div class="divs6">
      <center><div class="images6">
           <img src="<?php echo get_template_directory_uri(); ?>/img/cara.png" class="imgs6">
           <h1 class="txtsblue">VANESSA</h1>
           <P>Malibu, CA<br>
            Tour: 2/10/15</P> 
            <h1 class="reviews">LEAVE A REVIEW</h1>
        </div></center>
        <div class="texts6">
          <b>Overall rating: <i class="fas txtsblue fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas txtsblue  fa-star"></i><i class="fas txtsblue   fa-star"></i><i class="fas txtsblue  fa-star"></i></b></b>
          <h1 class="txtsblue">Lovely experience, I gladly recommend</h1>
         <p>The tour was great. I think your company did a great job setting expectations and preparing us for the tour (e.g. packing<br> tips , guidebooks, audio tour, etc.) otherwise, we would not have enjoyed it as much as we did.</p>
        </div>
    </div>
    </div>
    </div>
    </section>
    </div>
    <div class="container1">
    <section id="">
       <div class="flexboxs7">
    <div class="boxs7">
      <div class="divs7">
        <br>
    <input  class="input-tamano" placeholder="Name" type="text" name="">
    <br>
    <br>
    <input class="input-tamano"  placeholder="Email" type="text" name="">
    <br>
    <br>
    <input class="input-tamano" placeholder="Website" type="text" name="">
    <br>
    <br>
    <input class="input-msj"  placeholder="Message" type="text" name="">
    <br>
    <br>
    <button type="button" class="btns7 btn-outline-warning">SEND</button>
    </div>
    </div>
     </div>
    </section>
     </div>
     <br>
    <br>
    <br>
<section id="">
  <div class="flexboxs8">
    <div class="boxs8">
      <div class="marg">
        <div id="imagen" class="bc">
   <div id="info">
     <p id="headline">EVERY<br>DAY<br>A<br>DIFFERENT<br>JOURNEY</p>
   </div>
 </div>
 </div>
    </div>
   <div class="boxs8">
      <div class="divs8">
        <div id="imagen" class="bc2">
   <div id="info">
     <p id="headline">EVERY<br>DAY<br>A<br>DIFFERENT<br>JOURNEY</p>
   </div>
 </div>
      </div>
  </div>
  <div class="boxs8">
      <div class="marg">
        <div id="imagen" class="bc3">
   <div id="info">
     <p id="headline">EVERY<br>DAY<br>A<br>DIFFERENT<br>JOURNEY</p>
   </div>
 </div>
      </div>
  </div>
  <div class="boxs8">
      <div class="divs8">
        <div id="imagen" class="bc4">
            <div id="info">
              <p id="headline">EVERY<br>DAY<br>A<br>DIFFERENT<br>JOURNEY</p>
            </div>
        </div>
      </div>
  </div>
 </div>
</section>
     <section id="">
       <div class="flexboxs9">
    <div class="boxs9">
      <center><h1 class="bordes9">NEWSLETTER SUBSCRIPTION</h1></center>
      <div class="container1">
      <div class="divs9">
        <div class="texts9">
          <p class="txts">Sign up to our newsletter to recive directly on your email our news latest offers:</p>
          <div class="input">
            <i class="fas fa-envelope"></i>
             <input class="msj"  placeholder="" type="text" name="">
             
             <button type="button" class="btns9 btn-outline-warning">SUBSCRIBE</button>
             </span>
          </div>
          <br>
          <br>
          <br>
          <div class="divlist">
            <ul>
              <li class="listas9"><i class="fas icons9 fa-dollar-sign"></i><p class="aling">Go into the monthly draw to win a $500 travel voucher when signing up with us.</p></li>
               <br>
              <li class="listas9"><i class="fas icons9 fa-gift"></i><p class="aling">Go into the monthly draw to win a $500 travel voucher when signing up with us.</p></li>
               <br>
              <li class="listas9"><i class="fas icons9 fa-unlock"></i><p class="aling">Go into the monthly draw to win a $500 travel voucher when signing up with us.</p></li>
            </ul>

            </div>
        </div>
    </div>
     </div>
     <img src="<?php echo get_template_directory_uri(); ?>/img/9.png" class="imgs9 ct-u-z-index3">
     


    
    </div>
      <!-- <div class="div9">
        <img src="img/9.png" class="img9">
      </div> -->
    </div>
    </section>
    </div>

 <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css1/single.css">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Source+Sans+Pro" rel="stylesheet">

     

 <link href="https://fonts.googleapis.com/css?family=Raleway:400,700&display=swap" rel="stylesheet">
  <link href="css/fontawesome/css/all.min.css" rel="stylesheet" />
  


<script type="text/javascript">
  $(document).ready(function(){
  
  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  })

})
</script>






<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action( 'woocommerce_single_product_summary' );
		?>
	</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
